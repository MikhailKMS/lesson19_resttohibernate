package ru.sber.jd.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.sber.jd.dto.ULDto;
import ru.sber.jd.services.ULService;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class ULController {

    private final ULService ulService;

    @PostMapping
    public ULDto save(@RequestBody ULDto ulDto) {
        return ulService.save(ulDto);

    }

    @GetMapping
    public List<ULDto> findAll() {
        return ulService.getAll();
    }

    @GetMapping("/{id}")
    public ULDto findById(@PathVariable Integer id) {
        return ulService.getById(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        ulService.delete(id);
    }

    @PutMapping
    public ULDto update(@RequestBody ULDto ulDto) {
        return ulService.update(ulDto);

    }


}
