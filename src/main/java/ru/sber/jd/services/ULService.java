package ru.sber.jd.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.sber.jd.dto.ULDto;
import ru.sber.jd.entities.ULEntity;
import ru.sber.jd.repositories.ULRepository;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@RequiredArgsConstructor
public class ULService {

    private final ULRepository ulRepository;

    public ULDto save(ULDto dto) {
        ULEntity ulEntity = mapToEntity(dto);
        ULEntity savedEntity = ulRepository.save(ulEntity);
        return mapToDto(savedEntity);
    }

    public List<ULDto> getAll() {
        return StreamSupport.stream(ulRepository.findAll().spliterator(), false)
                .map(this::mapToDto)
                .collect(Collectors.toList());
    }

    public ULDto getById(Integer id) {
        return mapToDto(ulRepository.findById(id).orElse(new ULEntity()));
    }

    public void delete(Integer id) {
        ulRepository.deleteById(id);
    }

    public ULDto update(ULDto dto) {
        ULEntity ulEntity = mapToEntity(dto);
        ULEntity savedEntity = ulRepository.save(ulEntity);
        return mapToDto(savedEntity);
    }



    private ULEntity mapToEntity(ULDto dto) {
        ULEntity entity = new ULEntity();
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setInn(dto.getInn());
        return entity;
    }

    private ULDto mapToDto(ULEntity bookEntity) {
        ULDto dto = new ULDto();
        dto.setId(bookEntity.getId());
        dto.setName(bookEntity.getName());
        dto.setInn(bookEntity.getInn());
        return dto;
    }


}
