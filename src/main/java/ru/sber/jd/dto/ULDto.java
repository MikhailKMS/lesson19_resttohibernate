package ru.sber.jd.dto;

import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Id;

@Data
public class ULDto {

    @Column
    @Id
    Integer id;
    @Column
    String name;
    @Column
    Long inn;

}
